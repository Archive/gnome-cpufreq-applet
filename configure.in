AC_INIT(src/cpufreq-applet.c)
AM_CONFIG_HEADER(config.h)
AM_INIT_AUTOMAKE(gnome-cpufreq-applet, 0.3.1)

AM_MAINTAINER_MODE

AC_PROG_INTLTOOL

AC_ISC_POSIX
AC_PROG_CC
AC_STDC_HEADERS
AM_PROG_LIBTOOL
X_LIBS="$X_LIBS $X_PRE_LIBS -lX11 $X_EXTRA_LIBS"
AC_SUBST(X_LIBS)

GNOME_COMPILE_WARNINGS

dnl Stuff we might need, not quite sure yet

GLIB_REQUIRED=2.4.0
GTK_REQUIRED=2.4.0
LIBGNOME_REQUIRED=2.0.0
LIBGNOMEUI_REQUIRED=2.0.0
GNOME_VFS_REQUIRED=1.9.16
GCONF_REQUIRED=1.1.11
SCROLLKEEPER_REQUIRED=0.1.4
LIBPANEL_REQUIRED=2.5.0
LIBGLADE_REQUIRED=2.0.0


if test "${prefix}" = "NONE"; then
   prefix=/usr
fi

CFLAGS="$CFLAGS -Wall"
AC_SUBST(CFLAGS)

dnl configure argument fo disable schema install

AC_ARG_ENABLE(install_schemas,
 	[  --disable-install-schemas        Disable installation of the gconf schemas])
AM_CONDITIONAL(INSTALL_SCHEMAS, test x$enable_install_schemas != xno)


dnl ******************************************
dnl ** pkg-config dependacy checks          **
dnl ******************************************

PKG_CHECK_MODULES(GNOME_APPLETS, gtk+-2.0 >= $GTK_REQUIRED libpanelapplet-2.0 >= $LIBPANEL_REQUIRED)
AC_SUBST(GNOME_APPLETS_CFLAGS)
AC_SUBST(GNOME_APPLETS_LIBS)

PKG_CHECK_MODULES(GNOME_LIBS2, libgnome-2.0 >= $LIBGNOME_REQUIRED libgnomeui-2.0 >= $LIBGNOMEUI_REQUIRED)
AC_SUBST(GNOME_LIBS2_CFLAGS)
AC_SUBST(GNOME_LIBS2_LIBS)

PKG_CHECK_MODULES(CPUFREQ_SELECTOR, glib-2.0 >= $GLIB_REQUIRED gobject-2.0 >= $GLIB_REQUIRED)
AC_SUBST(CPUFREQ_SELECTOR_CFLAGS)
AC_SUBST(CPUFREQ_SELECTOR_LIBS)

build_gnome_vfs_applets=false
PKG_CHECK_MODULES(GNOME_VFS_APPLETS, 
		  [gnome-vfs-2.0 >= $GNOME_VFS_REQUIRED],
		  build_gnome_vfs_applets=true,)
AC_SUBST(GNOME_VFS_APPLETS_CFLAGS)
AC_SUBST(GNOME_VFS_APPLETS_LIBS)
AM_CONDITIONAL(BUILD_GNOME_VFS_APPLETS, $build_gnome_vfs_applets)

PKG_CHECK_MODULES(LIBGLADE, libglade-2.0 >= $LIBGLADE_REQUIRED)
AC_SUBST(LIBGLADE_CFLAGS)
AC_SUBST(LIBGLADE_LIBS)

AC_PATH_PROG(GDK_PIXBUF_CSOURCE, gdk-pixbuf-csource, no)

if test x"$GDK_PIXBUF_CSOURCE" = xno; then
  AC_MSG_ERROR([gdk-pixbuf-csource executable not found in your path - should be installed with GTK])
fi 
  
AC_SUBST(GDK_PIXBUF_CSOURCE)


AC_PATH_PROG(GCONFTOOL, gconftool-2, no)

if test x"$GCONFTOOL" = xno; then
  AC_MSG_ERROR([gconftool-2 executable not found in your path - should be installed with GConf])
fi

AM_GCONF_SOURCE_2

dnl =====================================================
dnl Check if build frequency selector
dnl =====================================================

AC_ARG_ENABLE(frequency-selector,
	      [  --enable-frequency-selector        Enable build Frequency Selector (default yes)], ,
	      enable_selector=yes)
AM_CONDITIONAL(BUILD_SELECTOR, test x$enable_selector = xyes)

if [[ x$enable_selector = x ]]
then
   enable_selector=no
else
   enable_selector=yes
fi

dnl ask whether to suid root the frequency selector executable on installation

AC_ARG_ENABLE(suid,
 	      [  --enable-suid[=ARG]      suid root [default=yes] the frequency selector executable (default yes)], ,
	      suid=yes)

AC_MSG_CHECKING(if we suid root cpufreq-selector at install)
if [[ x$enable_selector = xno ]]
then
   suid=no
else
   if [[ x$suid = x ]]
   then
      suid=no
   else
      suid=yes
   fi	
fi

AC_MSG_RESULT($suid)

AC_SUBST(suid)
AM_CONDITIONAL(SUID, test x$suid = xyes)


dnl Still need to decide on the checks for these

dnl ************************
dnl **   Set up gettext   **
dnl ************************
ALL_LINGUAS="de eu fr nb"

GETTEXT_PACKAGE=gnome-cpufreq-applet
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE, "$GETTEXT_PACKAGE", [The gettext package])

AM_GLIB_GNU_GETTEXT

# this is the directory where the *.{mo,gmo} files are installed
gnomelocaledir='${prefix}/${DATADIRNAME}/locale'
AC_SUBST(gnomelocaledir)

dnl ******************************************
dnl ** Check for Scrollkeeper               ** 
dnl ******************************************

AC_PATH_PROG(SCROLLKEEPER_CONFIG, scrollkeeper-config, no)
if test x$SCROLLKEEPER_CONFIG = xno; then
  AC_MSG_ERROR(Couldn't find scrollkeeper-config, please install the Scrollkeeper 0.1.4 package)
fi
AC_SUBST(SCROLLKEEPER_REQUIRED)

dnl ******************************************
dnl ******************************************


dnl ******************************************
dnl **  Test whether jw is installed        **
dnl ******************************************

AC_PATH_PROG(JW, jw, no)
if test x$JW = xno; then
  HAVE_JW="no"
else
  HAVE_JW="yes"
fi
AM_CONDITIONAL(HAVE_JW, test "x$HAVE_JW" = "xyes")
AC_SUBST(HAVE_JW)


# Honor aclocal flags
ACLOCAL="$ACLOCAL $ACLOCAL_FLAGS"



AC_DEFINE_UNQUOTED(GNOME_ICONDIR, "${prefix}/share/pixmaps", [Pixmap directory])

#defined the below to enable help to work for applets 

AC_DEFINE_UNQUOTED(DATADIR, "${prefix}/share", [Data Directory])
AC_DEFINE_UNQUOTED(SYSCONFDIR, "${prefix}/etc", [System Config Directory])
AC_DEFINE_UNQUOTED(LIBDIR, "${prefix}/lib", [Library Directory])
AC_DEFINE_UNQUOTED(PREFIX, "$prefix", [Prefix Directory])

AC_OUTPUT([ 
Makefile
gnome-cpufreq-applet.spec
po/Makefile.in
src/Makefile
src/cpufreq-selector/Makefile
help/Makefile
help/C/Makefile
])

echo "
gnome-cpufreq-applet-$VERSION:

        prefix:                               ${prefix}
        
        Install frequency selector:           ${enable_selector}
        Install frequency selector suid root: ${suid}
"

